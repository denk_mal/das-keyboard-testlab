#!/usr/bin/env python3
# coding=utf-8

import json
import logging
import os
import re
import sys
import time

import named_pipes


# get some base paths to load definition files
FULLPATH_SCRIPT = os.path.abspath(sys.argv[0])
BASEDIR_SCRIPT = os.path.dirname(FULLPATH_SCRIPT)
FILENAME_SCRIPT = os.path.basename(FULLPATH_SCRIPT)


class ServiceTalker:
    """
    this is a communication layer between an application and the das-keyboard service.
    """

    def __init__(self) -> None:
        """Open the pipes to the backend service."""
        self.communicationId = ""
        self.pid = ""
        self.firmwareVersion = ""
        self.mapping = None
        self.max_id = 0
        self.profile = {}
        tmpdir = "/tmp"
        pipe_read = os.path.join(tmpdir, 'daskeyboard_pipe_out')  # from service
        pipe_unsol = os.path.join(tmpdir, 'daskeyboard_pipe_unsolicited')
        pipe_write = os.path.join(tmpdir, 'daskeyboard_pipe_in')  # to service
        self.pipe_handler = named_pipes.NamedPipes([pipe_read, pipe_unsol], [pipe_write])

    @staticmethod
    def __convert_le_be__(in_str: str, size: int) -> str:
        """helperfunction; convert litte endian to big endian."""
        result = in_str
        while size > 1:
            result = ""
            for idx in range(0, len(in_str), size * 2):
                result += in_str[idx + size:idx + size * 2]
                result += in_str[idx:idx + size]
            size //= 2
            in_str = result
        return result

    def init_from_service(self) -> bool:
        """
        get information from current keyboard

        get some information from service like communication id, keyboard firmware version
        and firmware version.

        Parameters
        ----------
        Returns : boolean
            true if no errors occured and the information are valid
        """
        data = self.pipe_handler.execute_command("0000000001000d")
        if not data:
            logging.fatal("no response from service.")
            sys.exit(1)
        data = data[14:]  # strip header
        logging.debug("received following data: '%s'", data)
        if len(data) <= 16:
            logging.fatal("no device connected.")
            return False
        self.communicationId = data[4:12]
        self.pid = self.__convert_le_be__(data[0:4], 2)
        self.firmwareVersion = "%d.%d.%d" % (int(data[12:14], 16), int(data[14:16], 16), int(data[16:18], 16))
        if self.pid == '2020':
            with open(os.path.join(BASEDIR_SCRIPT, "keyboard_defs", "DK5QPID.json")) as json_file:
                self.mapping = json.load(json_file)
        elif self.pid == '202B':
            with open(os.path.join(BASEDIR_SCRIPT, "keyboard_defs", "X50QPID.json")) as json_file:
                self.mapping = json.load(json_file)
        elif self.pid == '2037':
            with open(os.path.join(BASEDIR_SCRIPT, "keyboard_defs", "DK4QPID.json")) as json_file:
                self.mapping = json.load(json_file)
        if not self.mapping:
            return False
        # get highest key id
        size = 0
        for key in self.mapping["ids"]:
            idx = int(key)
            if idx > size:
                size = idx
        # preset profile with passive black color, no active key settings
        self.max_id = size
        for idx in range(1, self.max_id+1):
            key = format(idx, '02x')
            self.profile[key] = ["01%s000000" % key, "c0%s" % key]
        return True

    def end_transfer(self) -> None:
        """confirm previous settings as valid."""
        cmd_prefix = self.communicationId + "0100"
        self.pipe_handler.execute_command(cmd_prefix + "ff")

    def close_service(self) -> None:
        """close communication with the das-keyboard."""
        self.pipe_handler.close_pipes("0000000001000d")

    def get_dimensions(self) -> dict:
        """get dimension of current keyboard (from json file)"""
        dimension = self.mapping["dimension"]
        ret_val = {"left": dimension[0][0], "top": dimension[0][1], "width": dimension[1][0], "height": dimension[1][1]}
        return ret_val

    def set_key(self, key, passive_effect: str = "", passive_color: str = "",
                active_effect: str = "", active_color: str = "", is_profile: bool = False) -> None:
        """
        set effect and color for passive and active state of a key

        This will set the effect and color for the passive and active state of the key. If the parameter is not set
        it will use the values from the profile. Calling this function only with the key parameter will 'restore'
        the profile settings for this key.

        **Attention:** for now the caller has to watch out if the effect uses a color or not.
        Calling with an effect that dosn't needs a color results in unforeseen effects!

        Parameters
        ----------
        key : str
            The key wich should be changed (2 char hex string)
        passive_effect : str, optional
            The effect of the unpressed key (2 char hex string); defaults to current profile
        passive_color : str, optional
            The color of the unpressed key (6 char hex string; RRGGBB); defaults to current profile
        active_effect : str, optional
            The effect of the unpressed key (2 char hex string); defaults to current profile
        active_color : str, optional
            The color of the unpressed key (6 char hex string; RRGGBB); defaults to current profile
        is_profile : boolean, optional
            save the new settings into the profile
        """
        cmd_prefix = self.communicationId + "0100"  # what stands this '0100' for ?
        key = key.lower()
        cmd_ini = "40%s" % key
        cmd1 = "%s%s%s" % (passive_effect, key, passive_color)
        cmd2 = "%s%s%s" % (active_effect, key, active_color)
        if not active_effect:
            cmd2 = self.profile[key][1]
        if is_profile and cmd1:
            self.profile[key] = [cmd1, cmd2]
        if not passive_effect:
            cmd1 = self.profile[key][0]
        self.pipe_handler.execute_command(cmd_prefix + cmd_ini)
        self.pipe_handler.execute_command(cmd_prefix + cmd1)
        self.pipe_handler.execute_command(cmd_prefix + cmd2)

    def set_effect(self, effect: str = "02", color: str = "000000") -> None:
        """
        set effect for whole keyboard.

        The effect will not be permanently; A call to end_transfer() will drop the effect!

        **Attention:** for now the caller has to watch out if the effect uses a color or not.
        Calling with an effect that doesn't needs a color results in unforeseen effects!

        Parameters
        ----------
        effect : str, optional
            The effect of the keyboard (2 char hex string); defaults to set static color
        color : str, optional
            The color of the effect (6 char hex string; RRGGBB); defaults to black
        """
        cmd_prefix = self.communicationId + "0100"  # what stands this '0100' for ?
        cmd1 = "%s%s" % (effect, color)
        self.pipe_handler.execute_command(cmd_prefix + cmd1)

    def set_profile(self, color: str, effect: str) -> None:
        """
        set effect and color for all keys on the keyboard and store as profile

        **Attention:** for now the caller has to watch out if the effect uses a color or not.
        Calling with an effect that doesn't needs a color results in unforeseen effects!

        Parameters
        ----------
        effect : str
            The effect of the key (2 char hex string)
        color : str
            The color of the effect (6 char hex string; RRGGBB)
        """
        for idx in range(1, self.max_id+1):
            key = format(idx, '02x')
            self.set_key(key, effect, color, is_profile=True)

    def normalize_keycode(self, input_value: str) -> str:
        """
        returns the keycode for the input value

        try to find out if the value is an id, a named key or a coordinate and returns the
        key code (from the json file) if it was a valid parameter

        Parameters
        ----------
        input_value :  str
            id, name or coordinate of the key
        Returns : str
            id of the key (2 char hex string); "00" if the key was not valid
        """
        id_regex = re.compile(r'^1?\d?\d$')
        name_regex = re.compile(r'^KEY_[A-Z0-9_]+$')
        coord_regex = re.compile(r'^(\d?\d),(-1|[0-6])$')
        input_value = input_value.strip()
        norm_value = None
        try:
            if id_regex.match(input_value):
                norm_value = self.mapping['ids'][input_value]
            elif name_regex.match(input_value):
                norm_value = self.mapping['names'][input_value]
            elif coord_regex.match(input_value):
                dimension = self.mapping['dimension']
                coords = self.mapping['coords']
                x = int(input_value.split(',')[0])
                y = int(input_value.split(',')[1])
                if (dimension[0][0] <= x <= dimension[1][0]) and (dimension[0][1] <= y <= dimension[1][1]):
                    x -= dimension[0][0]
                    y -= dimension[0][1]
                    if coords[y][x] == 'A':
                        norm_value = input_value
        except KeyError:
            norm_value = None
        key_id = "00"
        if norm_value:
            for ids_key, ids_value in self.mapping['ids'].items():
                if ids_value == norm_value:
                    key_id = format(int(ids_key), '02x')
                    break
        return key_id

    def normalize_action(self, action: str) -> str:
        """
        returns the actioncode for the input value

        try to find out if the value is an id or a named action and returns the
        action code (from the json file) if it was a valid parameter

        Parameters
        ----------
        action : str
            id or name of the action
        Returns : str
            id of the action (2 char hex string); empty string if the action was not valid
        """
        id_regex = re.compile(r'^[\dA-F]{2}$')
        name_regex = re.compile(r'^[A-Z][A-Z_]+$')
        action = action.strip().upper()
        norm_value = ""
        try:
            if id_regex.match(action):
                norm_value = action
            elif name_regex.match(action):
                norm_value = self.mapping['actions'][action]
        except KeyError:
            norm_value = ""
        return norm_value.lower()


def main() -> None:
    """test function to show the possible settings

    shows the following settings (changes every 10 seconds):

        * static color; red
        * blinking color; green
        * breathing color: blue
        * color cycle
        * color cloud
    At the end the color will be set to static white color.

    """
    service_talker = ServiceTalker()
    if not service_talker.init_from_service():
        print("something went wrong with the service communication!")
        return
    action_effect = service_talker.normalize_action("EFFECT_SET")
    service_talker.set_effect()
    service_talker.set_effect(action_effect, "ff0000")
    time.sleep(10)
    action_effect = service_talker.normalize_action("EFFECT_BLINK")
    service_talker.set_effect()
    service_talker.set_effect(action_effect, "00ff00")
    time.sleep(10)
    action_effect = service_talker.normalize_action("EFFECT_BREATHE")
    service_talker.set_effect()
    service_talker.set_effect(action_effect, "0000ff")
    time.sleep(10)
    action_effect = service_talker.normalize_action("EFFECT_COLOR_CYCLE")
    service_talker.set_effect()
    service_talker.set_effect(action_effect)
    time.sleep(10)
    action_effect = service_talker.normalize_action("EFFECT_COLOR_CLOUD")
    service_talker.set_effect()
    service_talker.set_effect(action_effect)
    time.sleep(10)
    action_effect = service_talker.normalize_action("SET")
    service_talker.set_profile("ffffff", action_effect)
    service_talker.end_transfer()
    service_talker.close_service()


if __name__ == '__main__':
    logging.basicConfig(format='%(asctime)s (%(levelname)s): %(message)s', datefmt='%F %T', level=logging.WARNING)
    main()
