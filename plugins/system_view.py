#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import os
import psutil
import sys
import threading
import time

FULLPATH_SCRIPT = os.path.abspath(sys.argv[0])
BASEDIR_SCRIPT = os.path.dirname(FULLPATH_SCRIPT)
FILENAME_SCRIPT = os.path.basename(FULLPATH_SCRIPT)


def preferences_struct():
    """
    json preferences profile structure
    """
    return {
        "cpu_load": {
            "0": ["KEY_1", "00FF00", "FFFFFF"],
            "1": ["KEY_2", "00FF00", "FFFFFF"],
            "2": ["KEY_3", "00FF00", "FFFFFF"],
            "3": ["KEY_4", "00FF00", "FFFFFF"],
            "4": ["KEY_5", "FFFF00", "FFFFFF"],
            "5": ["KEY_6", "FFFF00", "FFFFFF"],
            "6": ["KEY_7", "FFA500", "FFFFFF"],
            "7": ["KEY_8", "FF0000", "FFFFFF"],
            "8": ["KEY_9", "FF0000", "FFFFFF"],
            "9": ["KEY_0", "FF0000", "FFFFFF"]
        },
        "lock_keys": {
            "caps_lock": ["KEY_CAPS_LOCK", "0000FF"],
            "num_lock": ["KEY_NUM_LOCK", "0000FF"]
        }
    }


class SystemView(threading.Thread):
    def __init__(self, service_handler, data, *args, **kwargs):
        logging.debug("startup %s" % type(self).__name__)
        super(SystemView, self).__init__(*args, **kwargs)
        self._stop_event = threading.Event()
        if not data:
            logging.fatal("no datasection in the config file for '%s'!" % type(self).__name__)
            raise ValueError("no datasection in the config file for '%s'!" % type(self).__name__)
        self.service_handler = service_handler
        self.data = data
        self.cpu_load_keys = {}

        for key in data['cpu_load']:
            key_data = data['cpu_load'][key]
            key_code = service_handler.normalize_keycode(key_data[0])
            if key_code == "00":
                logging.fatal("incorrect keycode '%s' in config file for '%s'!" % (key_data[0], type(self).__name__))
                raise ValueError("incorrect keycode '%s' in config file for '%s'!" % (key_data[0], type(self).__name__))
            key_data[0] = key_code
            self.cpu_load_keys[int(key)] = key_data
        self.cpu_load_key_step = 100 // len(self.cpu_load_keys)
        self.start()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):
        while not self.stopped():
            self.cpu_load()
            self.keylock_state()
            time.sleep(0.1)

    def cpu_load(self):
        """
        get the cpu load and display a bar
        """
        percent = int(psutil.cpu_percent() // self.cpu_load_key_step)
        for idx in range(len(self.cpu_load_keys)):
            key = self.cpu_load_keys[idx][0]
            if percent > idx:
                color = self.cpu_load_keys[idx][1]
            else:
                color = self.cpu_load_keys[idx][2]
            self.service_handler.set_key(key, "01", color)

    def keylock_state(self):
        """
        show state of the lock onto the key itselfs
        """
        pass

    @staticmethod
    def schedule_event():
        return {}


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s (%(levelname)s): %(message)s', datefmt='%F %T', level=logging.DEBUG)
    import das_service_talker as service_talker

    service_talker = service_talker.ServiceTalker()
    service_talker.init_from_service()
    profile = SystemView(service_talker, preferences_struct())
    time.sleep(300)
    service_talker.close_service()
