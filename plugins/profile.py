#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import logging
import math
import os
import sys
import threading
import time

FULLPATH_SCRIPT = os.path.abspath(sys.argv[0])
BASEDIR_SCRIPT = os.path.dirname(FULLPATH_SCRIPT)
FILENAME_SCRIPT = os.path.basename(FULLPATH_SCRIPT)


def preferences_struct():
    """
    json preferences profile structure
    """
    return {
        "function": "unique",
        "parameters": ["ffffff", "set"]
    }


class Profile(threading.Thread):
    def __init__(self, service_handler, data, *args, **kwargs):
        logging.debug("startup %s" % type(self).__name__)
        super(Profile, self).__init__(*args, **kwargs)
        self._stop_event = threading.Event()
        if not data:
            logging.fatal("no datasection in the config file for '%s'!" % type(self).__name__)
            raise ValueError("no datasection in the config file for '%s'!" % type(self).__name__)
        self.service_handler = service_handler
        self.data = data
        # build rainbow table
        self.rb_table = []
        for idx in range(0, 240):
            self.rb_table.append(int(round((((math.sin(math.radians((idx*360/240)-90))) + 1) * 255) / 2)))
        for idx in range(240, 361):
            self.rb_table.append(0)
        self.offset_inc = 0
        # build keycode matrix for faster access
        dim = self.service_handler.get_dimensions()
        top = dim["top"]
        width = int(dim["width"])
        height = int(dim["height"])
        self.keycode_matrix = [[None] * width for _i in range(height-top)]
        for x in range(0, width):
            for y in range(top, height):
                self.keycode_matrix[y][x] = self.service_handler.normalize_keycode("%d,%d" % (x, y))
        self.set_profile()

    def stop(self):
        self._stop_event.set()

    def stopped(self):
        return self._stop_event.is_set()

    def run(self):
        offset = 0
        while not self.stopped():
            self._rainbow('01', offset)
            time.sleep(0.1)
            offset = (offset + self.offset_inc) % 360

    def set_profile(self):
        """get method name and parameter from the config file and execute it"""
        method_to_call = getattr(self, self.data["function"])
        method_to_call(self.data["parameters"])

    def unique(self, parameters: [str]) -> None:
        """set single color
            parameter : color [, effect]
        """
        color = parameters.pop(0)
        cmd = "SET"
        if parameters:
            cmd = parameters.pop(0)
        cmd = self.service_handler.normalize_action(cmd)
        self.service_handler.set_profile(color, cmd)
        self.service_handler.end_transfer()

    def rainbow(self, parameters: [str]) -> None:
        """set rainbow
            parameter : [effect [, offset]]
        """
        cmd = "SET"
        if parameters:
            cmd = parameters.pop(0)
        offset = 0
        if parameters:
            offset = int(parameters.pop(0))
        cmd = self.service_handler.normalize_action(cmd)
        self._rainbow(cmd, offset)

    def rainbow_flow(self, parameters: [str]) -> None:
        """set flowing rainbow
            parameter : [step_offset]
        not working for now; backend das-keyboard service is too slow!"""
        logging.info("start loop")
        increment = -5
        if parameters:
            increment = int(parameters.pop(0))
        self.offset_inc = increment
        self.start()

    def _rainbow(self, cmd, offset):
        dim = self.service_handler.get_dimensions()
        top = dim["top"]
        width = int(dim["width"])
        height = int(dim["height"])
        step = 360/width
        for x in range(0, width):
            angle = x*step
            red = self.rb_table[int((angle + 120 + offset) % 360)]
            green = self.rb_table[int(angle + offset) % 360]
            blue = self.rb_table[int((angle + 240 + offset) % 360)]
            color = "%s%s%s" % (format(red, '02x'), format(green, '02x'), format(blue, '02x'))
            for y in range(top, height):
                key = self.keycode_matrix[y][x]
                if key != '00':
                    self.service_handler.set_key(key, cmd, color, is_profile=True)
        self.service_handler.end_transfer()

    @staticmethod
    def schedule_event():
        return {}


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s (%(levelname)s): %(message)s', datefmt='%F %T', level=logging.DEBUG)
    import das_service_talker as service_talker

    service_talker = service_talker.ServiceTalker()
    service_talker.init_from_service()
    profile = Profile(service_talker, preferences_struct())
    profile.rainbow_flow('')
    service_talker.close_service()
