#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import imaplib
import logging
import os
import time
import threading
import sys

FULLPATH_SCRIPT = os.path.abspath(sys.argv[0])
BASEDIR_SCRIPT = os.path.dirname(FULLPATH_SCRIPT)
FILENAME_SCRIPT = os.path.basename(FULLPATH_SCRIPT)


class IMAP(object):
    """
    small helper class to handle imap communication
    """
    def __init__(self, host, user, passwd):
        if not host or not user or not passwd:
            raise ValueError("parameter must not be empty!")
        self.connection = imaplib.IMAP4_SSL(host)
        self.connection.login(user, passwd)

    def check_for_new_mail(self, box):
        """
        check if there are unseen emails in the named box

        Parameters
        ----------
        box : str
            IMAP name of the folder that should be checkd e.g. 'INBOX' or 'INBOX.subfolder'

        Returns : bool
            True if there is/are unseen email(s); False otherwise
        """
        self.connection.select(box)
        _result, data = self.connection.uid('search', None, "(UNSEEN)")  # search and return uids instead
        if data[0]:
            return True
        return False

    def __del__(self):
        try:
            self.connection.shutdown()
        except AttributeError:
            pass


def preferences_struct():
    """
    json preferences profile structure
    """
    return {
      "host": "mail.company.invalid",
      "username": "user@company.invalid",
      "password": "not_secure",
      "check_intervall": "15",
      "effect": "BLINK",
      "color": "FF0000",
      "bg_effect": "SET",
      "bg_color": "00FF00",
      "boxes": {
        "INBOX": [
          "KEY_INSERT",
          "KEY_HOME",
          "KEY_PAGE_UP"
        ],
        "INBOX.Google": [
          "KEY_DELETE",
          "KEY_END",
          "KEY_PAGE_DOWN"
        ]
      }
    }


class MailWatcher(object):
    def __init__(self, service_handler, data):
        logging.debug("startup %s" % type(self).__name__)
        if not data:
            logging.fatal("no datasection in the config file for '%s'!" % type(self).__name__)
            raise ValueError("no datasection in the config file for '%s'!" % type(self).__name__)
        self.data = data
        self.tr_started = False
        self.tr = None
        self.results = {}
        self.old_state = {}
        self.check_intervall = 300
        for key in self.data:
            if key.endswith('effect'):
                self.data[key] = service_handler.normalize_action(self.data[key])
            if key.endswith('color'):
                self.data[key] = self.data[key].lower()
        if 'bg_effect' not in data:
            data['bg_effect'] = None
        if 'bg_color' not in data:
            data['bg_color'] = None
        self.check_intervall = int(self.data['check_intervall'])
        for box in self.data['boxes']:
            self.old_state[box] = False
            new_list = []
            for keycodes in self.data['boxes'][box]:
                new_list.append(service_handler.normalize_keycode(keycodes))
            self.data['boxes'][box] = new_list

    def check_mail(self):
        """
        iterate over multiple boxes
        """
        self.results = {}
        conn = IMAP(self.data['host'], self.data['username'], self.data['password'])
        for box in self.data['boxes']:
            self.results[box] = conn.check_for_new_mail(box)
        del conn

    def stop(self):
        if self.tr and self.tr.isAlive():
            self.tr.join()

    def schedule_event(self):
        """
        to avoid waitings for the imap server we start a background task for imap checking;
        if it is already startet, we check if it still running or if it is ready.

        Parameters
        ----------
        Returns : list
            a list of changed check results if the task is ready. If the states haven't changed
            according to the last check results then the list is empty.
            On thread startup and on non finished thread run the list is always empty.
        """
        ret_val = {}
        if self.tr_started:
            if self.tr.is_alive():
                logging.debug("thread still running")
            else:
                logging.debug("thread is ready")
                self.tr = None
                self.tr_started = False
                for box, state in self.results.items():
                    if self.old_state[box] != state:
                        if state:
                            template = [self.data['effect'], self.data['color'],
                                        self.data['bg_effect'], self.data['bg_color']]
                        else:
                            template = []
                        self.old_state[box] = state
                        for key in self.data['boxes'][box]:
                            ret_val[key] = template
        else:
            logging.debug("start thread")
            self.tr_started = True
            self.tr = threading.Thread(target=self.check_mail, args=())
            self.tr.start()
        return ret_val

    # for standalone-only run task
    def _run_task(self):
        while True:
            try:
                logging.debug("result: '%s'", self.schedule_event())
                time.sleep(self.check_intervall)
            except Exception as ex:
                print("error occured! (%s)" % ex)
                time.sleep(60)


if __name__ == "__main__":
    logging.basicConfig(format='%(asctime)s (%(levelname)s): %(message)s', datefmt='%F %T', level=logging.DEBUG)
    import das_service_talker as service_talker

    service_talker = service_talker.ServiceTalker()
    service_talker.init_from_service()
    try:
        obj = MailWatcher(service_talker, preferences_struct())
        obj._run_task()
    except KeyboardInterrupt:
        service_talker.close_service()
