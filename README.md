# das-keyboard

This is a small application that puts some functionality onto the led lightning
on a Das-Keyboard. It has been written to avoid the usage of the cloud-based
Q-Software. 

**This software is provided as-is**

## tl;dr
The package is splitted into three sections; The plugins, that are doing the core
jobs of getting information and setting the leds of the keyboard, the backend
service talker that talks to the keyboard driver and the main application, that
coordinates between plugins and service talker and doing some init stuffs.
The main app is starting the plugins every 5 sec in separate threads and
waits for the response of all threads. 


## Copyright / License

Copyright 

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
