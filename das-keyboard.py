#!/usr/bin/env python3
# coding=utf-8

import argparse
import importlib
import glob
import json
import logging
import os
import pathlib
import sys
import time
from multiprocessing.dummy import Pool

# import keyboard handler service
import das_service_talker as service_talker

FULLPATH_SCRIPT = os.path.abspath(sys.argv[0])
BASEDIR_SCRIPT = os.path.dirname(FULLPATH_SCRIPT)
FILENAME_SCRIPT = os.path.basename(FULLPATH_SCRIPT)
PLUGIN_FOLDER = "plugins"


def init_class(plugin_name: str):
    """load and initialise objects from the plugin folder.

        This loads the plugin named by plugin_name; The classname inside of the
        pluginfile is a capitalized string of the plugin filename.
        E.g. mail_watcher -> MailWatcher; git_alert -> GitAlert, profile -> Profile

        Every plugin class needs a ```__init__()``` function that get the service_talker and the
        config section for the plugin.

        Parameters
        ----------
        plugin_name : str
            name of the plugin to load

        Returns : object
            the loaded object
    """
    objectname = ''.join(x.capitalize() or '_' for x in plugin_name.split('_'))
    try:
        obj = importlib.import_module("%s.%s" % (PLUGIN_FOLDER, plugin_name))
    except AttributeError:
        logging.error("Plugin named '%s' not loaded!", plugin_name)
        return None
    return getattr(obj, objectname)


class DasKeyboard:
    """ This class connects to the das-keyboard service (via read/write pipes), loads the config file,
        loads the plugins and schedules the plugins.
    """
    def __init__(self, config_file, preaction = False):
        """get a connection to the das-keyboard service and loads the config file.
        the config file in .local/share/das-keyboard.sh is a simple container for config parts
        of the plugins e.g like this
        '''
        {
          "profile": {
            ...
          },
          mail_watcher: {
            ...
          }
        }
        '''
        """
        self.__schedule_tasks = []
        self.service_handler = service_talker.ServiceTalker()
        if not self.service_handler.init_from_service():
            logging.critical("coult not connect service")
            sys.exit(1)
        with open(config_file) as json_file:
            self.data = json.load(json_file)

        if preaction:
            self.service_handler.set_effect(self.service_handler.normalize_action("EFFECT_COLOR_CLOUD"))
            time.sleep(30)  # not necessary; it's only to show on the keyboard that the process has started

    def init_plugins(self):
        """scan plugin folder for .py files, load this module and initialise it.
            If a plugin is called 'profile' it will call the set_profile() function only;
            all other plugins are added to the schedule task table that will be called
            repeatedly.
        """
        for filename in glob.glob1(os.path.join(BASEDIR_SCRIPT, PLUGIN_FOLDER), "*.py"):
            name = filename[:-3]
            logging.debug("try to initialize plugin: %s", name)
            c = init_class(name)
            if c:
                if name in self.data:
                    s = c(self.service_handler, self.data[name])
                else:
                    s = c(self.service_handler, None)
                self.__schedule_tasks.append(s)

    @staticmethod
    def _trigger_event(task):
        return task.schedule_event()

    def event_loop(self):
        """call the schedule_event() function by using the multiprocessing function of python."""
        try:
            while True:
                pool = Pool(4)  # handle up the worker processes for the plugins
                # start all plugins and wait for all processes to finished
                results = pool.map(self._trigger_event, self.__schedule_tasks)
                key_was_set = False
                # iterate over the results and set any keys
                for items in results:
                    for key, values in items.items():
                        self.service_handler.set_key(key, *values)
                        key_was_set = True
                if key_was_set:
                    self.service_handler.end_transfer()
                pool.close()  # close all plugin threads
                pool.join()  # wait until all plugins stopped
                time.sleep(5)  # wait 5 sec for the next round
        except KeyboardInterrupt:
            for task in self.__schedule_tasks:
                task.stop()
            self.service_handler.close_service()

def parseArguments():
    config_path = os.path.join(os.path.expanduser("~/.local/share"), os.path.splitext(FILENAME_SCRIPT)[0])
    config_file = os.path.join(config_path, 'das-keyboard.json')
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--config", default=config_file, help="change default configuration location")
    parser.add_argument("-p", "--preaction", action="store_true",
                        help="execute a ligining feedback on the keyboard before startung up")
    parser.add_argument("-v", "--verbosity", action="count", default=0, help="increase verbosity")
    args = parser.parse_args()
    level = logging.ERROR
    if args.verbosity == 1:
        level = logging.WARNING
    elif args.verbosity == 2:
        level = logging.INFO
    elif args.verbosity >= 3:
        level = logging.DEBUG
    logging.basicConfig(format='%(asctime)s (%(levelname)s): %(message)s', datefmt='%F %T', level=level)
    logging.info("Set logging.level to %s" % logging.getLevelName(level))
    return args

def main():
    args = parseArguments()
    config_file = pathlib.Path(args.config).expanduser()
    main_object = DasKeyboard(config_file, args.preaction)
    main_object.init_plugins()
    main_object.event_loop()

if __name__ == '__main__':
    main()
